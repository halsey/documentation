# Logging and Monitoring

I used the EFK (Elasticsearch, Fluentd, Kibana) stack for logging and monitoring.
Below you can see a bit of the user interface.

![](kibana1.png)
![](kibana2.png)