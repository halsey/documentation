# CI/CD pipeline with Buddy

## Build

I chose [Buddy](buddy.works) to run the testing, continuous integration and deployment.

This is Buddy's UI

![](buddy-ci.png)

There are four projects, `discounts`, `database`, `products` and `elk-deployment` (which contains the Kubernetes files).

Instead of passing the whole continuous integration pipeline, you can use the already tested and built images on [Docker Hub](https://hub.docker.com/u/medigun).

## Deployment

### Application

Inside the repositories of the services you will find the Kubernetes files to run the deploy. Just clone or copy each and follow the instructions below.

### EFK Stack

You will need [this repository](https://gitlab.com/halsey/elk-deployment). It contains all the necessary files and configurations to get the whole application up and running. 

### Running

Inside the cluster, do

```
kubectl create -f <file name>
```

with all the files. If you feel like modifying a pod or service, just edit the file and then

```
kubectl apply -f <file name>
```

