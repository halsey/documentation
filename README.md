# Hash SRE Test

This is a test for the position of SRE at Hash Lab, in which I was asked to build and deploy the provided application in a Kubernetes cluster with logging and monitoring.

Below is a diagram of the architecture I formed to solve the case.

![](K8S.png)

I chose Google Kubernetes Engine as the provider, used Terraform to provision the infrastructure, Buddy to run tests and the CI/CD pipeline and the EFK stack for logging and monitoring.