# Cluster Provisioning with Terraform

I chose Terraform to provision a cluster on Google Kubernetes Engine, as Hash already uses it and it's a robust and quick form to provision infrastructure

To create the cluster you will need to clone [this repository](https://gitlab.com/halsey/terraform-create-app). You will need to change the Service Account file path to yours.

Verify if you have a working Terraform environment. If not, just go to [this link](https://www.terraform.io/downloads.html) and download the package of your choice.

Cd into the cloned repository and do

```
terraform init
terraform apply
```

Verify if the requested changes are ok, if so, write `yes` and press enter.